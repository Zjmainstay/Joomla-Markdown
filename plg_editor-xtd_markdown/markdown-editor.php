<?php
    defined('_MARKDOWN_EDITOR') or define('_MARKDOWN_EDITOR', 1);

    //Joomla Framework
    define('_JEXEC', 1);

    $base = realpath(dirname(__FILE__) . '/../../../administrator');

    if (file_exists($base . '/defines.php'))
    {
        include_once $base . '/defines.php';
    }

    if (!defined('_JDEFINES'))
    {
        define('JPATH_BASE', $base);
        require_once JPATH_BASE . '/includes/defines.php';
    }

    require_once JPATH_BASE.'/includes/framework.php';
    require_once JPATH_BASE.'/includes/helper.php';
    require_once JPATH_BASE.'/includes/toolbar.php';

    // Instantiate the application.
    $app = JFactory::getApplication('administrator');

    // Initialise the application.
    $app->initialise(
        array('language' => $app->getUserState('application.lang'))
    );
    //End Joomla Framework

    $name = JRequest::getVar('name');
    $lang = Jfactory::getLanguage();
    $lang->load('plg_editors-xtd_markdown');

    if($app->isAdmin()){
        //load mootools
        include('static/tpl/editor.tpl.php');
    }
?>