<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.markdown
 *
 * @copyright   Copyright (C) 2012 Hayato Sugimoto. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Editor Readmore buton
 *
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.readmore
 * @since       1.5
 */
class plgButtonMarkdown extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	 * readmore button
	 * @return array A two element array of (imageName, textToInsert)
	 */
	public function onDisplay($name)
	{
		$doc = JFactory::getDocument();
		$getContent = $this->_subject->getContent($name);
		$setContent = str_replace('\'content\'', 'content', $this->_subject->setContent($name,'content'));
		$js = "
			function getMarkdownContent(){
				return $getContent;
			}
			function setMarkdownContent(content){
				$setContent;
			}
		";
		$doc->addScriptDeclaration($js);
		$link = '../plugins/editors-xtd/markdown/markdown-editor.php';
		JHtml::_('behavior.modal');
		$button = new JObject;
		$button->modal = true;
		$button->link = $link;
		$button->text = JText::_('PLG_EDITORS_XTD_MARKDOWN_BUTTON');
		$button->name = 'picture';
		$button->options = "{handler: 'iframe', size: {x: 800, y: 500}}";
		return $button;
	}
}
