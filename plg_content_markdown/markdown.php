<?php
/**
 * @version		$Id: highlighter.php 2010-03-01 19:34:56Z ian $
 * @package		Hayatos
 * @copyright	Copyright (C) 2005 - 2011  Hayatos. All rights reserved.
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgContentMarkdown extends JPlugin {

	public function onContentPrepare($context, &$article, &$params, $page = 0) {

		//check whether a content has a syntax highlighter code or not
		if (stripos($article->text, 'class="markdown-doc') === false) {
			return;
		}

		$article->text = preg_replace('/<pre class="markdown-doc">([\s\S]*)<\/pre>/i','<div class="markdown-doc">$1</div>', $article->text);

		// Use this plugin only in site application
		if (JFactory::getApplication()->isSite())
		{
			$plg_markdown_editor_path = JURI::root()  . 'plugins/editors-xtd/markdown';
			$plg_markdown_content_path = JURI::root() . 'plugins/content/markdown';
			$doc = JFactory::getDocument();
			if($this->params->get('include_jquery')) $doc->addScript($plg_markdown_editor_path . '/static/js/jquery-1.8.3.min.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/google-code-prettify/prettify.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/pagedown/Markdown.Converter.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/pagedown/Markdown.Sanitizer.js');
			$doc->addScript($plg_markdown_content_path . '/Markdown.Editor.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/pagedown/local/Markdown.local.zh.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/Markdown.Extra.js');
			$doc->addScript($plg_markdown_editor_path . '/static/editor/css_browser_selector.js');
			$doc->addScript($plg_markdown_content_path . '/editor.js');
			//MathJax for Math Expression
			// $mathjax = 'MathJax.Hub.Config({ tex2jax: { inlineMath: [["$","$"], ["\\(","\\)"]], processEscapes: true },  messageStyle: "none"});';
			// $doc->addScriptDeclaration($mathjax);
			// $doc->addScript($plg_markdown_editor_path . '/static/editor/mathJax.js');
			// $doc->addScript('http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML');
			$doc->addStyleSheet($plg_markdown_editor_path . '/static/editor/google-code-prettify/prettify.css');
		}

		return true;
	}

}
